## Modules description

The project is separated by two modules:
* Execution module `payaraserver`
    
    * The execution module consists of one maven dependency which holds the Payara Micro server API.
    Its perpouse is to deploy the `application` in the IDE and provide debugging.
    
* Application module `application`

    * The `application` module holds the application itself.
    It has all the needed JavaEE dependencies including a maven plugin which builds the standalone `.jar`.
    This `.jar` includes the Payara Micro server and the deployment (i.e. `application` module).
    
## The build plugin

The build configuration should look like this:
```XML
    <packaging>war</packaging>
    <build>
        <finalName>ROOT</finalName>
        <plugins>
            <plugin>
                <groupId>fish.payara.maven.plugins</groupId>
                <artifactId>payara-micro-maven-plugin</artifactId>
                <version>1.0.1</version>
                <executions>
                    <execution>
                        <id>bundle</id>
                        <phase>package</phase>
                        <goals>
                            <goal>bundle</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <payaraVersion>${version.payara}</payaraVersion>
                    <deployWar>true</deployWar>
                </configuration>
            </plugin>
        </plugins>
    </build>
```
* The `packing` property needs to be `war` as the build plugin will need to include it as a deployment in the core Payara server.

* The `finalName` property is with the value `ROOT` in order for the context path of the deployment to be   `http://localhost:8080/`.
Any other name would result in `http://localhost:8080/finalName/`

* If you want to run the built package, look for `ROOT-microbundle.jar` in the target directory of `application`

## Configuring JavaEE

### Configuring CDI
In order for CDI to work you need to add `beans.xml` inside `resources/META-INF/`.
The base file contains the following content:
```XML
    <?xml version="1.0" encoding="UTF-8"?>
    <beans
            xmlns="http://xmlns.jcp.org/xml/ns/javaee"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee 
                          http://xmlns.jcp.org/xml/ns/javaee/beans_1_1.xsd"
            bean-discovery-mode="all">
    </beans>
```
The JavaEE dependency is:
```XML
        <dependency>
            <groupId>javax</groupId>
            <artifactId>javaee-api</artifactId>
            <version>8.0</version>
            <scope>provided</scope>
        </dependency>
```
If there is deployment failure due to missing web.xml, the default content of the file should be:
```XML
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee"
         xmlns:web="http://java.sun.com/xml/ns/javaee"
         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
         version="3.0">
</web-app>
```

### Configuring JPA

This application uses embedded H2 database.

The `persistence.xml` (located under `resources/META-INF`) should look like this:
```XML
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<persistence xmlns="http://xmlns.jcp.org/xml/ns/persistence" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.1" xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence http://xmlns.jcp.org/xml/ns/persistence/persistence_2_1.xsd">
    <persistence-unit name="applicationDB" transaction-type="JTA">
        <jta-data-source>java:global/MyDS</jta-data-source>
        <exclude-unlisted-classes>false</exclude-unlisted-classes>
        <properties>
            <property name="javax.persistence.schema-generation.database.action" value="create"/>
        </properties>
    </persistence-unit>
</persistence>
```
* The datasource is specified in the `web.xml file`

Define the datasource in the web.xml file:
```xml
    <data-source>
        <name>java:global/MyDS</name>
        <class-name>org.h2.Driver</class-name>
        <database-name>applicationDB</database-name>
        <url>jdbc:h2:file:~/applicationDB</url>
        <user>sa</user>
        <password>password</password>
    </data-source>
```

> NOTE: The field `password` cannot be empty.
On initial database creation the provided password will be set as default for the `sa` user.

### Configuring JSF

The web.xml configuration should look like this:
```XML
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee"
         xmlns:web="http://java.sun.com/xml/ns/javaee"
         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
         version="3.0">

    <listener>
        <listener-class>com.sun.faces.config.ConfigureListener</listener-class>
    </listener>
    <!-- Change to "Production" when you are ready to deploy -->
    <context-param>
        <param-name>javax.faces.PROJECT_STAGE</param-name>
        <param-value>Development</param-value>
    </context-param>

    <!-- JSF mapping -->
    <servlet>
        <servlet-name>Faces Servlet</servlet-name>
        <servlet-class>javax.faces.webapp.FacesServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>

    <servlet-mapping>
        <servlet-name>Faces Servlet</servlet-name>
        <url-pattern>*.xhtml</url-pattern>
    </servlet-mapping>
</web-app>
```

* As dependencies `javax-api` is enough but `com.sun.faces.config.ConfigureListener` has to be imported as a listener class so ` javax.faces.context.FacesContextFactory` can be found.
Despite the IDE showing that the class cannot be found, the message can be ignored.

> NOTE: When importing `@ViewScoped` on a bean, import `javax.faces.view.ViewScoped` to use it with `@Named`.
Otherwise the other `@ViewScoped` is imported it will not work with `@Named` but with `javax.faces.bean.ManagedBean`.
This class is deprecated and `@Named` is the way to go.

### Configuring MicroProfile
The MicroProfile configurations should work as shown in the documentations.
The dependency used in this project is:
```XML
        <dependency>
            <groupId>org.eclipse.microprofile</groupId>
            <artifactId>microprofile</artifactId>
            <version>2.0.1</version>
            <type>pom</type>
            <scope>provided</scope>
        </dependency>
```

> The `microprofile-config.properties` file should be under `resources/META-INF/` in order to work.

### IDE Configuration
To run the project in your IDE, set `io.github.vvbs.StartUp` as main class and the parent directory as working directory.
The classpath of module `payaraserver` should be selected.
You should also run `mvn package` on every new change before running `payaraserver`.