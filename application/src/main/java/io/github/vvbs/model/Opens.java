package io.github.vvbs.model;

import javax.persistence.*;

@Entity
@NamedQueries({
        @NamedQuery(name = Opens.FIND_OPEN, query = "SELECT o FROM Opens o WHERE o.pageName = :open")
})
public class Opens {

    public static final String FIND_OPEN = "findOpen";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private int openings;

    private String pageName;

    public Opens() {
    }

    public Opens(String pageName) {
        this.pageName = pageName;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public void setOpenings(int openings) {
        this.openings = openings;
    }

    public int getOpenings() {
        return openings;
    }

    public Integer getId() {
        return id;
    }
}
