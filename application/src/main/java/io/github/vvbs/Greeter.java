package io.github.vvbs;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;

public class Greeter {

    @Inject
    @ConfigProperty(name = "personName")
    private String personName;

    public String getPersonName(){
        return personName;
    }
}
