package io.github.vvbs.dao;

import io.github.vvbs.model.Opens;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Optional;

@ApplicationScoped
public class OpeningsDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Opens saveOpenings(Opens opens) {
        entityManager.merge(opens);
        return opens;
    }

    public Optional<Opens> findOpenByPageName(String pageName) {
        Query query = entityManager.createNamedQuery(Opens.FIND_OPEN);
        query.setParameter("open", pageName);

        return query.getResultList().stream().findFirst();
    }
}
