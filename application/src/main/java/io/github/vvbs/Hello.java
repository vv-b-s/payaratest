package io.github.vvbs;

import io.github.vvbs.dao.OpeningsDAO;
import io.github.vvbs.model.Opens;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/hello")
public class Hello {

    @Inject
    private Greeter greeter;

    @Inject
    private OpeningsDAO openingsDAO;

    @GET
    public Response sayHello() {
        Opens o = openingsDAO.findOpenByPageName("hello").orElse(new Opens("hello"));
        o.setOpenings(o.getOpenings() + 1);
        String greetMessage = String.format("Hello, %s!\nThe page was opened %d times.", greeter.getPersonName(), o.getOpenings());

        openingsDAO.saveOpenings(o);
        return Response.ok(greetMessage).build();
    }


}
