package io.github.vvbs;

import io.github.vvbs.dao.OpeningsDAO;
import io.github.vvbs.model.Opens;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Named
@ViewScoped
public class IndexBean implements Serializable {
    @Inject
    private OpeningsDAO openingsDAO;

    @NotNull
    private String name;
    private String greetMessage;
    private int numberOfOpenings;

    @PostConstruct
    private void init() {
        Opens open = openingsDAO.findOpenByPageName("index").orElse(new Opens("index"));
        open.setOpenings(open.getOpenings() + 1);
        numberOfOpenings = open.getOpenings();

        openingsDAO.saveOpenings(open);
    }

    public void submitGreet() {
        greetMessage = String.format("Hello %s! This page was opened %d times.", name, numberOfOpenings);
    }

    public String getGreetMessage() {
        return greetMessage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
