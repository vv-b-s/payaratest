package io.github.vvbs;

import fish.payara.micro.BootstrapException;
import fish.payara.micro.PayaraMicro;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class StartUp {
    public static void main(String[] args) throws BootstrapException {
        PayaraMicro instance = PayaraMicro.getInstance();
        getDeploymentWars().forEach(instance::addDeploymentFile);
        instance.bootStrap();
    }

    private static List<File> getDeploymentWars() {
        List<File> wars = new ArrayList<>();
        Queue<File> foldersToOpen = new LinkedList<>(Arrays.asList(new File(new File("").getAbsolutePath())));

        while (foldersToOpen.size() > 0) {
            File folder = foldersToOpen.remove();
            foldersToOpen.addAll(Arrays.stream(folder.listFiles())
                    .filter(f -> f.isDirectory())
                    .collect(Collectors.toList()));

            for (File file : folder.listFiles()) {
                if (folder.getName().equals("target") && file.getName().endsWith(".war")) {
                    wars.add(file);
                }
            }
        }

        return wars;
    }
}
